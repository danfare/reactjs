'use strict';

var React = require('react');
var ReactDOM = require('react-dom');

var TestForm = React.createClass({
  getInitialState: function() {
    return {
      val: null
    };
  },

  handleInputChange: function(key, event) {
    var partialState = {};
    partialState[key] = event.target.value;
    this.setState(partialState);
  },

  render: function() {
    var val = this.state.val;

    return (
      <label>
        val: <input type="text" value={val} onChange={this.handleInputChange.bind(null, 'val')} />{val}
      </label>
    );
  }
});

ReactDOM.render(
  <TestForm />,
  document.getElementById('form-ctn')
);

var ExampleApplication = React.createClass({
  render: function() {
    var elapsed = Math.round(this.props.elapsed  / 100);
    var seconds = elapsed / 10 + (elapsed % 10 ? '' : '.0' );
    var message =
      'React has been successfully running for ' + seconds + ' seconds.';

    return <p>{message}</p>;
  }
});

var start = new Date().getTime();

setInterval(function() {
  ReactDOM.render(
    <ExampleApplication elapsed={new Date().getTime() - start} />,
    document.getElementById('container')
  );
}, 50);